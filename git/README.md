Terminology
=====
## repositoy
The thing you create for every new project, kind of like your project folder
## commit
Telling git that the current stage of the files should be versioned, you can later access every commit and restore it's stage. This means that you should commit at least after every minor milestone and you need to commit before pushing your changes to the remote for others to use. Every commit needs a message describing what has changed after the last commit.
You can enter your message in an editor:  
`git commit`  
This will open a file in your default editor. Just add your commit messaage in the first lines, save and exit
Or specify the message directly:  
`git commit -m="implemented getter and setter methods"`  

Actions you could take
=====
Bringing your project folder to git
-----
Assuming you have a directoy in which all your important files regarding this project are in 📂
If you want to enable a version history using git or share the project with others via [GitLab](https://gitlab.com) or [GitHub](https://github.com), you can easyly create a new git repositoy for it.

1. Make sure that [git](https://git-scm.com/) is installed and navigate into your project directory
2. Initialize a new repository in this directory:  
   `git init`  
3. Make sure to add a `.gitignore` file to exclude sensitive information (like passwords or API keys), compiled files (like .class files in Java) and libraries (like node_modules)
4. Add a new remote called `origin` (this is the default name for remotes, just use this unless you have a reason not to)  
   `git remote add origin gitlab.com/namespace/reponame`  
   Where `gitlab.com` is the provider, `namespace`is usually your username and `reponame` is the slug for your repositoy (should generally unly contain lowercase characters, numbers and -). You can also see this link when visiting The repository online in the url-bar or when clicking the clone button
5. Add all the files for usage with git:  
   `git add .`  
6. Commit your changes (you need to specify a commit message)  
   `git commit`  
7. Push your changes to the remote and set the default remote and branch  
   `git push --set-upstream origin master`
   aka
   `git push -u origin master`  
   This will upload your changes to the remote and also specify your default remote and branch for future pushs
   `master` is our branch (which is also a default which every repository should have). I recommend doing the first commit on the master branch even if you plan to (and should) switch to a `dev` branch later on, because you generally don't want your `dev` branch protected.
8. Create a new branch for development and check it out (i.e. use this branch for new commits). You should generally only have functional 'releases' on the `master` branch  
   `git branch dev && git checkout dev`  
9. Set the same defaults for the dev branch  
   `git push --set-upstream origin dev`
   aka
   `git push -u origin dev`  
