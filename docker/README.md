# Docker 101
See [docker-101](https://gitlab.com/torbeneims/docker-101)
# Look here first
If everything else fails, try `sudo snap install docker`

### Install
As root:
```bash
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/raspbian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
# Set up Docker's Apt repository:
echo   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/raspbian \
"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" |   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo docker run hello-world
```

Add to docker Group
```bash
sudo usermod -aG docker <user>
```
Then, restart the shell session.

### Install compose
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

### Uninstall Docker Engine
List installed docker packages
```bash
dpkg -l | grep -i docker
```
uninstall
```bash
sudo apt-get purge docker docker-ce docker-ce-cli docker-ce-rootless-extras docker-scan-plugin
```

### Purge
Look [here](https://askubuntu.com/a/1021506/1611042)
