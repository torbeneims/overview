## Attach Block storage
1. Attach through WebUI & the three commands provided there
2. List mountable iSCSI devices
```sh
sudo fdisk -l
```
3. Mount
```sh
sudo mount /dev/sdb /mnt/vault
```