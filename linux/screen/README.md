## (Start) and/or atttach
```
screen -RR -D <session-name>
```

## Run command (unverified)
```
screen -S -X
```

## Multiuser Support
Press `Ctrl+A` before each command starting with `:`
```
:multiuser on
:acladd <user>
```

Attach with
```
screen -x <user>/<session>
```
[Stackexchange Article](https://unix.stackexchange.com/questions/163872/sharing-a-terminal-with-multiple-users-with-screen-or-otherwise)
