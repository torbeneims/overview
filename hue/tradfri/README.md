## Pair Tradfri accessory with a single zigbee-compatible light (Hue, Müller-Licht)
1. `Press the link button` on the smart control `4x` within `5sec`
> The accessory should flash, reset and then start pulsing. Wait until the accessory is pulsing.
2. Turn the light's `power off and on` again (to enter touchlink mode)
3. Move the accessory close to the light (approx. `5cm`) and `hold the link button`.
> The light should start flashing.
4. Release the pairing button once the light stopped blinking and the accessorie's indicator turns off

## Pair Tradfri accessory with a single or multiple lights (using a zigbee gateway - Hue, Tradfri gateway)
1. `Press the link button` on the smart control `4x` within `5sec`
> The accessory should flash, reset and then start pulsing. Wait until the accessory is pulsing.
2. Move the accessory close to the gateway and `hold the link button`
3. Enable touchlink on the gateway using a third party app like Hue Essentials
> The accessory's indicator light should turn off, pulse and turn off again.
4. Release the link button after the accessory's indicator light turns off again.
5. Repeat steps `2. to 4.` for `pairing a single light`