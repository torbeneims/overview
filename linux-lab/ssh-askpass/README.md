
Add to environment of /etc/environment
```
SSH_ASKPASS_REQUIRE="prefer"
SSH_ASKPASS="~/.ssh/askpass.sh"
```

Add to ~/.zshrc
`alias llssh='SSH_ASKPASS_REQUIRE="prefer" SSH_ASKPASS="~/.ssh/askpass.sh" ssh'`
