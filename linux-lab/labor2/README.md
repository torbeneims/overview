## PostgreSQL install
```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install postgresql
```
## Config
*/etc/postgresql/14/main/postgresql.conf*
```
60: listen_addresses = '*'                  # what IP address(es) to listen on;
95: password_encryption = scram-sha-256     # scram-sha-256 or md5
```
```
# restart service
sudo service postgresql restart
# or sudo systemctl restart postgresql
```

```
# create user & db
sudo su - postgres
psql
CREATE USER gnuhealth # doesn't work, ignore
CREATE DATABASE health WITH OWNER=gnuhealth # doesn't work
^d
exit
```

# user
```
sudo su - postgres
createuser --interactive --pwprompt
psql
ALTER USER gnuhealth WITH PASSWORD 'xxx'; # doesn't work
^d
createdb health --owner=gnuhealth
# if you need it: dropdb
# list dbs: psql -l
exit
```
```sh
echo "host    health          gnuhealth       linuxlab-app05.lxd      scram-sha-256" >> /etc/postgresql/14/main/pg_hba.conf

# restart service
sudo service postgresql restart
```



@app
```
# test connection
psql -h linuxlab-db05.lxd -U gnuhealth -d health
^d
```

## nmap
```
sudo apt-get install nmap

# Check if port is open
nmap -p 5432 linuxlab-db05.lxd
```

## Create user
```
sudo useradd --shell=/usr/sbin/nologin --home-dir /opt/gnuhealth gnuhealth
#sudo useradd -s /usr/sbin/nologin -d /opt/gnuhealth gnuhealth
```


```sh
sudo apt install pip virtualenv libpq-dev
sudo virtualenv /opt/gnuhealth/venv
sudo chown -R gnuhealth:gnuhealth /opt/gnuhealth

# proxy
export http_proxy=http://web-proxy.rrzn.uni-hannover.de:3128
export https_proxy=http://web-proxy.rrzn.uni-hannover.de:3128

# proxy alternative
exit
# or ^d
```

Add to /etc/environment 
```
http_proxy=http://web-proxy.rrzn.uni-hannover.de:3128
https_proxy=http://web-proxy.rrzn.uni-hannover.de:3128
```

# Get into the venv
```sh
sudo su - gnuhealth -s /bin/bash
source venv/bin/activate # to get in to the venv
pip install gnuhealth-all-modules uwsgi # --proxy args don't work

mkdir -R /opt/gnuhealth/{etc,var/{log,lib}}
```

```sh
# user@local
scp gnuhealth.service gnuhealth_log.conf trytond.conf uwsgi_trytond.ini llapp:~/
# ubuntu@app
sudo mv * /opt/gnuhealth/etc
sudo chown gnuhealth:gnuhealth *
```

```sh
sudo su - gnuhealth -s /bin/bash
source venv/bin/activate
trytond-admin
^d
sudo mv /opt/gnuhealth/etc/gnuhealth.service /etc/systemd/system/
sudo systemctl start gnuhealth
sudo systemctl enable gnuhealth
```

```sh
sudo apt install net-tools
netstat -tulpen
sudo netstat -tulpen | grep uwsgi
```


## GNU Health-Client
```sh
virtualenv ~/gnuhealth-env # to add a venv
# possibly chown user:group ~/gnuhealth-env -R

sudo apt install python3-gi python3-gi-cairo gir1.2-gtk-3.0
source ~/gnuhealth-venv/bin/activate # to get in to the venv
pip install gnuhealth-client # don't sudo
```

### with X11
```sh
ssh -X app
source gnuhealth-venv/bin/activate # go into the virtual python env
gnuhealth-client
# graphical ui should start
# enter your password for gnuhealth 
```

### Datenbank mit initialisieren
/opt/gnuhealth/etc/trytond.conf
```
uri = postgresql://gnuhealth:<pass>@<db_url>:5432
```
(venv) gnuhealth@app
```sh
# initialize db:
trytond-admin -c /opt/gnuhealth/etc/trytond.conf -d health --all
```

(gnuhealth-venv) ubuntu@app
```sh
sudo systemctl stop gnuhealth
sudo systemctl start gnuhealth
```


