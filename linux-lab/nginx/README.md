# Nginx

> To help with nginx debugging, I have created the following aliases:
> ```sh
> alias ep='vim ~/nginx_connect.py'
> alias nc='sudo vim /etc/nginx/conf.d/default.conf'
> alias py='python ~/nginx_connect.py'
> alias rn='sudo systemctl restart nginx'
> ```
> (Press Esc > `:q!` to exit vim ;) )


## Configuration
u@web:/etc/nginx/conf.d/default.conf
```conf
server {
        listen 443 ssl;
        server_name linuxlab-web05.lxd localhost;
        server_tokens off;

        ssl_certificate         /etc/ssl/certs/linuxlab-web05.crt;
        ssl_certificate_key     /etc/ssl/private/linuxlab-web05.key;

        location ~ {
                proxy_pass https://linuxlab-app05.lxd:8443;
                proxy_ssl_certificate     /etc/ssl/certs/linuxlab-web05.crt;
                proxy_ssl_certificate_key /etc/ssl/private/linuxlab-web05.key;
        }
}
```

```sh
sudo systemctl restart nginx
```


## Common errors
```
ssl.SSLError: [SSL: WRONG_VERSION_NUMBER] wrong version number (_ssl.c:997)
```
==> You are trying to connect to an nginx instance with https that is not ready for https (check `ssl`, `ssl_certificate` and `ssl_certificate_key`)  
  
  
```
xmlrpc.client.ProtocolError: <ProtocolError for XXXXX: 400 Bad Request>
```
==> You must use https after requiring so in your nginx config  
  
  
```
xmlrpc.client.ProtocolError: <ProtocolError for admin:JonasFahrrad420@localhost:80/health/: 404 Not Found>
```
==> Nginx and query port mismatch  
  
  
```
nginx: [emerg] unexpected "}" in /etc/nginx/conf.d/default.conf:XX
```
==> You're likely missing a `;` in your config  
  
  
```
nginx[7691]: Enter PEM pass phrase:
nginx[7691]: Enter PEM pass phrase:
nginx[7691]: nginx: [emerg] cannot load certificate key "/etc/nginx/client.key": PEM_read_bio_PrivateKey() failed (SSL: error:1400006B:UI routines::processing error:while readin>Nov 16 22:28:48 linuxlab-web05 nginx[7691]: nginx: configuration file /etc/nginx/nginx.conf test failed
```
==> Double check if you have removed the key passphrase (`sudo openssl rsa -in <your_web>.key -out <your_web>.key`)  
  

# Verify if TLS is working
I have absolutely no clue if this is what we are supposed to do xd
any@any.lxd
```sh
openssl s_client -showcerts -connect linuxlab-app05.lxd:8443 </dev/null
openssl s_client -showcerts -connect linuxlab-web05.lxd:443 </dev/null
openssl s_client -showcerts -starttls postgres -connect linuxlab-db05.lxd:5432 </dev/null
```
