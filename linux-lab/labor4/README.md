
# Require the app's tryton to verify the postgresql's SSL certificate


u@app:/opt/gnuhealth/etc/trytond.conf
```sh
21: uri = postgresql://gnuhealth:henriksmum69@linuxlab-db05.lxd:5432?sslmode=require
```

u@app
```sh
sudo systemctl restart gnuhealth
```

## Move keys and change owner
u@app
```sh
sudo mv /etc/ssl/private/linuxlab-app05.key /etc/ssl/certs/
sudo chown gnuhealth:gnuhealth /etc/ssl/certs/linuxlab-app05.crt
sudo chown gnuhealth:gnuhealth /etc/ssl/private/linuxlab-app05.key
```

# Require HTTPS for app
u@app:/opt/gnuhealth/etc/uwsgi_trytond.ini
```sh
3: https = [::]:8443,/etc/ssl/certs/linuxlab-app05.crt,/etc/ssl/certs/linuxlab-app05.key,HIGH,/usr/local/share/ca-certificates/rootCA.crt
```
