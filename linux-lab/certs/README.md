# Creating your certificate authority (CA)
ubuntu@db
```sh
mkdir ~/ca
cd ~/ca
openssl req -x509 -sha256 -days 1825 -newkey rsa:2048 -keyout rootCA.key -out rootCA.crt
# Country Name (2 letter code) [AU]:DE
# State or Province Name (full name) [Some-State]:Niedersachsen
# Locality Name (eg, city) []:Hanover
# Organization Name (eg, company) [Internet Widgits Pty Ltd]:Torben
# Organizational Unit Name (eg, section) []:
# Common Name (e.g. server FQDN or YOUR name) []:linuxlab-db05.lxd
# Email Address []:
```

# Request and sign your local certificate
u@db:~/ca
```sh
openssl req -newkey rsa:2048 -keyout linuxlab-db05.key -out linuxlab-db05.csr
openssl rsa -in linuxlab-db05.key -out linuxlab-db05.key
openssl x509 -req -in linuxlab-db05.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out linuxlab-db05.crt -days 500 -sha256
```

```sh
mv linuxlab* /etc/postgresql/14/main/certs
cd /etc/postgresql/14/main/certs
sudo chown postgres:postgres linuxlab-*

# Request other certificates
u@app:~/certs
```sh
openssl req -newkey rsa:2048 -keyout linuxlab-app05.key -out linuxlab-app05.csr
openssl rsa -in linuxlab-db05.key -out linuxlab-db05.key
chmod og= *.key # make private key not world-readable
#scp linuxlab-app05.scr ubuntu@linuxlab-db05.lxd:~/certs
```
u@web:~/certs
```sh
openssl req -newkey rsa:2048 -keyout linuxlab-web05.key -out linuxlab-web05.csr
openssl rsa -in linuxlab-db05.key -out linuxlab-db05.key
chmod og= *.key
#scp
```

# Sign other certificates
u@db:~/ca
```sh
openssl x509 -req -in linuxlab-app05.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out linuxlab-app05.crt -days 500 -sha256
openssl x509 -req -in linuxlab-web05.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out linuxlab-web05.crt -days 500 -sha256

# !Move the app crt file on the app server and the web crt file on the web server:
#scp linuxlab-app05.crt ubuntu@linuxlab-app05.lxd:~/certs
#scp linuxlab-web05.crt ubuntu@linuxlab-web05.lxd:~/certs
```

`u@web:~/certs`
```sh
#Now on both, the web and app server, move the crt file to /etc/ssl/certs and the key file to /etc/ssl/private
sudo mv linuxlab-web05.crt /etc/ssl/certs
sudo mv linuxlab-web05.key /etc/ssl/private/
sudo systemctl restart postgresql
```

`u@app:~/certs`
```sh
sudo mv linuxlab-app05.crt /etc/ssl/certs
sudo mv linuxlab-app05.key /etc/ssl/private/
sudo systemctl restart gnuhealth
```

## See cert data
```sh
openssl x509 -noout -text -in linuxlab-db05.crt
```

# Checking connection
## Create a proteus virtual environment
u@web:~
```sh
sudo apt install pip virtualenv libpq-dev
#don't sudo
virtualenv proteus-venv
source proteus-venv/bin/activate
```

## Install proteus
### Setup proxy
u@web or (proteus-venv) u@web
```sh
export http_proxy=http://web-proxy.rrzn.uni-hannover.de:3128
export https_proxy=http://web-proxy.rrzn.uni-hannover.de:3128
echo "http_proxy=http://web-proxy.rrzn.uni-hannover.de:3128
https_proxy=http://web-proxy.rrzn.uni-hannover.de:3128" >> /etc/environment
```

(proteus-venv) u@web
```sh
pip install proteus
```

~/connect_to_server.py
```py
from proteus import config
print('Connecting to the server...')
conf = config.set_xmlrpc("http://admin:<YOUR_TRYTON_ADMIN_PASS>@linuxlab-app05.lxd:8000/health/")
print('Success')
```

(proteus-venv) u@web:~
```sh
python connect_to_server.py
```

(In case you chose a password with special characters:)
(venv) gnuhealth@app
```sh
trytond-admin -c /opt/gnuhealth/etc/trytond.conf -d health -p
```

## Copy the CA's private key to all three servers
`rootCA.crt` (on u@db:~/ca)
scp u@db:~/ca/rootCA.crt u@{app,db,web}:/usr/local/share/ca-certificates
```sh
sudo update-ca-certificates
```
