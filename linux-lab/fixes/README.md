### on web server
```sh
sudo rm /etc/nginx/sites-enabled/default
```

nano /etc/nginx/conf.d/default.conf
```ini
# Changes in location:
location ~ {
    # no changes at proxy pass

    # proxy_ssl_certificate and proxy_ssl_certificate_key can be removed

    # add the following lines
    proxy_ssl_trusted_certificate /usr/local/share/ca-certificates/rootCA.crt;
    proxy_ssl_verify on;
}
```
restart nginx

### on app server
sslmode in der protgres uri in /opt/gnuhealth/etc/trytond.conf anpassen
```ini
?sslmode=verify-full&sslrootcert=/usr/local/share/ca-certificates/rootCA.crt
```
restart gnuhealth
