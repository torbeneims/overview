# TLS for PostgreSQL

## Move keys
u@db:/etc/postgresql/14/main
```sh
cd /etc/postgresql/14/main
sudo mv ~/certs .
sudo chown postgres:postgres certs/linuxlab-db05.*
sudo chown postgres:postgres certs
```

## Set keys
/etc/postgresql/14/main/postgresql.conf on u@db
```conf
107: ssl_cert_file = '/etc/postgresql/14/main/certs/linuxlab-db05.crt'
110: ssl_key_file = '/etc/postgresql/14/main/certs/linuxlab-db05.key'
```

## Limit access to SSH connections
/etc/postgresql/14/main/pg_hba.conf on u@db
```
107: hostssl health          gnuhealth       linuxlab-app05.lxd      scram-sha-256
```
(change host to hostssl)


## Restart for changes to take effect
```u@db
sudo systemctl restart postgresql

# optionally see the status
sudo systemctl status postgresql@14-main.service
```

## Remove the key passphrase
```sh
openssl rsa -in linuxlab-db05.key -out linuxlab-db05.key
```

Change the python file to http://

## Check connection
(proteus-venv) u@web:~
```sh
python connect_to_server.py
```

if necessary, copy the rootCA.crt to u@app:/etc/ssl/certs



