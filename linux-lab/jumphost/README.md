### Connect
```sh
ssh -J user@your.jumphost:port user@your.server -p port
```

~/.ssh/config
```config
Host jumphost
	HostName your.jumphost
	Port XXXX
	User user
	
Host yourserver
	HostName your.server
	Port 22
	User user
	ProxyJump jumphost
```
```sh
ssh yourserver
```


```sh
sudo apt install fail2ban
sudo systemctl status fail2ban
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sudo systemctl restart fail2ban

# show banned ips
sudo fail2ban-client banned
```


### SSH Copy ID
```sh
ssh-keygen
ssh-copy-id [-i /path/to/your/id_rsa] yourserver
# Enter your password as often as asked (likely 3 times, wait for a new TOTP each time)
# Password: jumpserverpw
# Verification Code: TOTP
# user@your.server: serverpw
```

### Disable SSH PW
/etc/ssh/sshd_config
```
Port yourenewport
...
PasswordAuthentication no
```
```sh
sudo systemctl restart ssh
```
