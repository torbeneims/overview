ESP32 CP210x USB to UART Bridge VCP Drivers
===
[Espressif Site](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/establish-serial-connection.html#:~:text=CP210x%20USB%20to%20UART%20Bridge%20VCP%20Drivers)  
Use [CP210x Windows Drivers](https://www.silabs.com/documents/public/software/CP210x_Windows_Drivers.zip) (direct download)
