## 1. Change into your workspace directory
```sh
cd ~/catkin_ws/src
```

## 2. Create a new package
with the dependencies `roscpp`, `tf2_ros` and  `f2_geometry_msgs`
```sh
catkin_create_pkg tf_publisher roscpp tf2_ros tf2_geometry_msgs
```

## 3. Change into your project directory
```sh
cd tf_publisher 
```

## 4. Create your source file
```sh
touch src/transformer.cpp
```

And add the code
```cpp
#include <ros/ros.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <math.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "my_awesome_tf_broadcaster");
    tf2_ros::TransformBroadcaster br;
    geometry_msgs::TransformStamped out_tf;

    double alpha = 0.0;

    ros::Rate rate(4.0 /* Hz */);

    while (ros::ok()) {
        out_tf.header.stamp = ros::Time::now();
        out_tf.header.frame_id = "world";
        out_tf.child_frame_id = "base_link";
        out_tf.transform.translation.x = cos(alpha) * 1.0f;
        out_tf.transform.translation.y = sin(alpha) * 1.0f;
        alpha += 4.0 * 2 * M_PI / 360;
        if(alpha > 2 * M_PI)
            alpha = 0;
        out_tf.transform.rotation.w = 1;
        br.sendTransform(out_tf);

        ros::spinOnce();
        rate.sleep();
    }
}
```


## 6. Define the executables and nodes
into the `CMakeList.txt` file parallel to your package `src`
```
/* 136 */ add_executable(node src/transformer.cpp)

/* 142 */ set_target_properties(node PROPERTIES OUTPUT_NAME node PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
/* 146 */ add_dependencies(node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
/* 149 */ target_link_libraries(node
    ${catkin_LIBRARIES}
)
```

## 7. Create your launch file
```sh
touch src/launch/launch.launch
```

and define your launch configuration

```xml
<launch>
    <node pkg="tf_publisher" type="node" name="node" output="screen" />
</launch>
```

## 8. Build your package
```sh
cd ~/catkin_ws
catkin_make
```

## 8. Launch
```sh
roslaunch tf_publisher launch.launch
```

## 9. See the output
(In another terminal)
```sh
rostopic echo /tf
```
