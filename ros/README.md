## 1
```sh
 mkdir -p ~/catkin_ws/src
 ```

 ## 2
 ```sh
 cd ~/catkin_ws/
 catkin_make
 ```

 ## 3
 ```sh
 echo "source /home/$USER/catkin_ws/devel/setup.zsh" >> ~/.zshrc
 source ~/.zshrc 
 ```

# Install CLion
```sh
sudo snap install clion --classic
```

# Debug
### *Something* does not work
```sh
roscore
```
```sh
source ~/catkin_ws/devel/setup.zsh
```
