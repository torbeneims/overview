### Install
https://stackoverflow.com/a/19584407/11500248
1. [Download nvm](https://github.com/nvm-sh/nvm/blob/master/README.md#install--update-script)
```
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh -o install_nvm.sh
```
2. Install nvm
```
bash install_nvm.sh
```
3. List node versions
```
nvm list-remote
```
4. Install node
```
nvm install 16.1.0
```
5. Test installation
```
node -v
npm -v
```
