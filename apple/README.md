# iPad not booting
## Obvious steps
1. Charge the device for at least 1hr, disconnect power for at least 30min
2. Hold the Power button

## Reset (does not clear data)
1. Connect to power, tap `vol down`, then `vol up`, then hold the power button