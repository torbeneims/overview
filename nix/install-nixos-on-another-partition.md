**THIS CONFIGURATION IS BROKEN**

I have installed NixOS along with Windows installed on a single device and wish to reinstall NixOS on another partition:

Current disk layout:  
Windows EFI (105MB FAT) | Microsoft Reserved (17MB) | Windows (1.2TB NTFS) | Free Space (576GB) | UEFI (1.1GB FAT) | NixOS (270GB Ext4)

Goal disk layout:  
Windows EFI (105MB FAT) | Microsoft Reserved (17MB) | Windows (1.2TB NTFS) | UEFI (1GiB FAT) | NixOS (remaining space) | UEFI old (1.1GB FAT) | NixOS old (270GB Ext4)


## Partitioning
```bash
sudo parted

print free

# Create the boot partition
sudo parted /dev/nvme0n1 -- mkpart boot fat32 1153GB 1154GB
#                  start of the free block ---^^^^^^ ^^^^^^--- 1GB further
sudo parted /dev/nvme0n1 -- set 6 esp on
#       the partition number ---^

# Create the primary NixOS partition
torben@torbens-xps ~> sudo parted /dev/nvme0n1 -- mkpart nixos ext4 1154GB 1730GB
#                                              end of last block ---^^^^^^ ^^^^^^--- end of free space
```

## Formatting
```bash
sudo mkfs.ext4 -L nixos /dev/nvme0n1p7
sudo mkfs.fat -F 32 -n boot /dev/nvme0n1p6
```

## Mounting
```bash
sudo mount /dev/disk/by-label/nixos /mnt
sudo mkdir -p /mnt/boot
sudo mount /dev/disk/by-label/boot /mnt/boot
```

```bash
ls -lha /dev/disk/by-uuid/
```

```bash
sudo mount /dev/nvme0n1p7 /mnt
sudo mount /dev/nvme0n1p6 /mnt/boot

sudo nixos-enter

nixos-rebuild switch --flake gitlab:torbeneims/nixos#torbens-xps
```

## Adding old partition to grub
```bash
# find out the uuid:
ls -l /dev/disk/by-uuid/* | grep p4
```

Add to `configuration.nix`
```nix
boot.loader.grub.extraEntries = ''
    menuentry "NixOS Old" {
        search --set="nixosold" --fs-uuid 5CA0-2D8C
        configfile "($nixosold)/boot/grub/grub.cfg"
    }
'';
```

## Installation
```bash
sudo nixos-install --flake ~/nixos/flake.nix#torbens-xps --root /mnt
```

# Fixing it
Ich hab jetzt nochmal nen Rebuild mit --install-grub gemacht, das dürfte neu sein; dann natürlich die Disks anpassen hat bestimmt auch geholfen; und der Pfand von dem efi-File ist etwas weird, den habe ich jetzt nochmal in den Bootmanager hinzugefügt, vorher hatte er vermutlich bei der Installation das systemd-File hinzugefügt und dann nach dem Rebuild gab's aber nurnoch Grub

![Install fix 1](./install_fix_1.jpeg)
![Install fix 2](./install_fix_2.jpeg)
![Install fix 3](./install_fix_3.jpeg)